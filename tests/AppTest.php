<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AppTest extends TestCase
{

    /**
     * Login Test
     *
     * @return void
     */
    public function testLogin()
    {
        $this->visit('auth/login')
        ->see('Login');
    }

    public function testRegister()
    {
        $this->visit('auth/register')
        ->see('Register');
    }

}
