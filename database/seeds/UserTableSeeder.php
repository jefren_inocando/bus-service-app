<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Jefren Inocando',
            'email' => 'jefren.inocando@gmail.com',
            'password' => Hash::make('123456'),
        ]);

        User::create([
            'name' => 'Lebron James',
            'email' => 'lebron@james.com',
            'password' => Hash::make('123456'),
        ]);
    }

}
