<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

// Redirect root to home route
Route::get('/', ['middleware' => 'auth', function() {
    return redirect('/home');
}]);

// get users current location by browser's geolocation
Route::get('/home', ['middleware' => 'auth', function() {
    return view('nearby');
}]);

// find all nearby bus stops and display map
Route::post('/home', ['middleware' => 'auth', function() {
    $latitude = Input::get('latitude');
    $longitude = Input::get('longitude');

    // find all nearby bus stops name using google places API with user's browser geolocation
    $near = json_decode(@file_get_contents("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$latitude,$longitude&radius=500&types=bus_station&key=AIzaSyA4tp0iigEWDPpdLYrS95LxpC3hzrianVw"));

    // find corresponding bus stop id by bus stop name based on google places API results
    foreach ($near->results as $val) {

        $model = \App\BusStop::where('name', $val->name)->first();

        $nearby_bus_stops[] = [
            'bus_stop_id' => $model->bus_stop_id,
            'name' => $model->name,
        ];
    }

    return view('nearby-results', [
        'nearby_bus_stops' => $nearby_bus_stops,
        'latitude' => $latitude,
        'longitude' => $longitude,
    ]);
}]);

// search by bus stop id
Route::get('bus/{id}',['middleware' => 'auth', function ($id) {
    $model = \App\Bus::getData($id);

    return view('home', ['model' => $model]);
}]);

// Search by Bus Stop ID
Route::post('/bus', ['middleware' => 'auth', function() {
    $model = \App\Bus::getData(Input::get('busId'));

    return view('home', ['model' => $model]);
}]);

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
                        