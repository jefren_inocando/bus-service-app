<?php

namespace App;

/**
 * Get Bus Arrival Time By Bus Stop ID using transport.sg API
 *
 * @author Jefren Inocando <jefren.inocando@gmail.com>
 */
class Bus
{

    /**
     * Uses transport.sg API to get all buses information by bus stop id
     * 
     * @param type $busId
     * @return type string
     */
    public static function getData($busId)
    {
        $opts = [
            'http' => [
                'method' => "GET",
                'header' =>
                "AccountKey: x6d9WOYNReEs9ACRUAiVmQ==\r\n" .
                "UniqueUserID: 0a193730-f76d-4143-a69a-0c6fda31cf3b\r\n" .
                "accept: application/json\r\n"
            ]
        ];

        $context = stream_context_create($opts);

        $file = @file_get_contents("http://datamall2.mytransport.sg/ltaodataservice/BusArrival?BusStopID=$busId", false, $context);

        if ($file === FALSE) {
            return NULL;
        } else {
            return json_decode($file);
        }
    }

    /**
     * Get Estimated Arrival Time of bus
     * 
     * @param type $status
     * @param type $eta
     * @return string
     */
    public static function getEstimatedArrivalTimeInMinutes($status, $eta)
    {
        if ($status == 'Not In Operation') {
            return 'Bus service has ended';
        } else {
            if (!empty($eta)) {
                $time = round(abs(strtotime($eta) - time()) / 60);

                if ($time < 2) {
                    return 'Arriving';
                }

                return $time;
            } else {
                return 'No Data. Please Refresh.';
            }
        }
    }

}
