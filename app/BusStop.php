<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * BusStop Model to access bus_stops table which lists all bus stop data
 *
 * @author Jefren Inocando <jefren.inocando@gmail.com>
 */
class BusStop extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bus_stops';

}
