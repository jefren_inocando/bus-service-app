@extends('layouts.master')

@section('content')

<div class="row">

    <div class="panel panel-default">

        <div class="panel-heading">Search By Bus Stop Number</div>

        <div class="panel-body">

            {!! Form::open(['url' => 'bus']) !!}

            <div class="input-group">
                {!! Form::text('busId','', ['class' => 'form-control', 'placeholder' => 'Bus Stop Number']) !!}

                <span class="input-group-btn">
                    {!! Form::button('Search', ['class' => 'btn btn-primary','type' => 'submit']) !!}
                </span>
            </div>

            {!! Form::close() !!}

        </div>

    </div>

    @if(is_null($model))
    <div class="well well-lg text-center">
        <h1>No result. Please use a valid bus stop number.</h1>
    </div>
    @else
    <div class="well well-lg text-center">
        <h1>Your current bus stop number is: {{ $model->BusStopID }}</h1>
    </div>

    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th>Bus</th>
                <th>Estimated Arrival Time</th>
                <th>Estimated Arrival Time (Subsequent Bus)</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($model->Services as $value)
            <tr>
                <td>{{$value->ServiceNo}}</td>
                <td><?= App\Bus::getEstimatedArrivalTimeInMinutes($value->Status, $value->NextBus->EstimatedArrival) ?></td>
                <td><?= App\Bus::getEstimatedArrivalTimeInMinutes($value->Status, $value->SubsequentBus->EstimatedArrival) ?></td>
            </tr>
            @endforeach
        </tbody>
    </table>

    @endif
</div>

@endsection