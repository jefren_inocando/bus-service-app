@extends('layouts.master')

@section('content')

<div class="row">

    <h2 class="text-center">Please select nearest bus stop to view bus arrival times:</h2><hr>

    <table class="table table-condensed table-striped">
        <thead>
            <tr>
                <th>Nearest Bus Stops</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($nearby_bus_stops as $value)
            <tr>
                <td><a href="{{url('bus', ['id'=>$value['bus_stop_id']])}}">{{$value['name']}}</a></td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <script>
        var map;
        var infowindow;

        function initMap() {
            var current_location = {lat: <?= $latitude ?>, lng: <?= $longitude ?>};

            map = new google.maps.Map(document.getElementById('map'), {
                center: current_location,
                zoom: 17
            });

            infowindow = new google.maps.InfoWindow();

            var service = new google.maps.places.PlacesService(map);
            service.nearbySearch({
                location: current_location,
                radius: 500,
                types: ['bus_station']
            }, callback);
        }

        function callback(results, status) {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
                for (var i = 0; i < results.length; i++) {
                    createMarker(results[i]);
                }
            }
        }

        function createMarker(place) {
            var placeLoc = place.geometry.location;
            var marker = new google.maps.Marker({
                map: map,
                position: place.geometry.location
            });

            google.maps.event.addListener(marker, 'click', function () {
                infowindow.setContent(place.name);
                infowindow.open(map, this);
            });
        }

    </script>

    <div id="map"></div>

    <script src="https://maps.googleapis.com/maps/api/js?signed_in=true&libraries=places&callback=initMap" async defer></script>

</div>

@endsection