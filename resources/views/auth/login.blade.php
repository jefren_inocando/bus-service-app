@extends('layouts.master')

@section('content')

<div class="panel panel-default">

    <div class="panel-heading">Login</div>

    <div class="panel-body">

        {!! Form::open(array('url' => 'auth/login')) !!}

        <div class="form-group">
            {!! Form::label('email', 'Email') !!}
            {!! Form::email('email','', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('password', 'Password') !!}
            {!! Form::password('password', ['class' => 'form-control']) !!}
        </div>

        <div class="checkbox">
            <label>
                {!! Form::checkbox('remember') !!} Remember Me
            </label>
        </div>

        <div class="form-group">
            {!! Form::button('Login', ['class' => 'btn btn-primary','type' => 'submit']) !!}
        </div>

        {!! Form::close() !!}

    </div>

</div>

<div class="alert alert-info">
    <h2>Test Accounts:</h2>

    <table class="table">
        <thead>
            <tr>
                <th>Email</th>
                <th>Password</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>jefren.inocando@gmail.com</td>
                <td>123456</td>
            </tr>
            <tr>
                <td>lebron@james.com</td>
                <td>123456</td>
            </tr>
        </tbody>
    </table>
</div>

@endsection