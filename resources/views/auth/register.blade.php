@extends('layouts.master')

@section('content')

<div class="panel panel-default">

    <div class="panel-heading">Register</div>

    <div class="panel-body">
        
        {!! Form::open(array('url' => 'auth/register')) !!}
        
        <div class="form-group">
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name','', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('email', 'Email') !!}
            {!! Form::email('email','', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('password', 'Password') !!}
            {!! Form::password('password', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('password_confirmation', 'Confirm Password') !!}
            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::button('Register', ['class' => 'btn btn-primary','type' => 'submit']) !!}
        </div>
        
        {!! Form::close() !!}
        
    </div>

</div>

@endsection