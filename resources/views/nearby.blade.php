@extends('layouts.master')

@section('content')

<h1 class="text-center">Searching Nearby Bus Stops...</h1>

<div class="spinner center-block"></div>

<div class="hidden">

    {!! Form::open(['url' => 'home','id' => 'nearbyForm']) !!}

    {!! Form::hidden('latitude','', ['id' => 'latitude']) !!}
    {!! Form::hidden('longitude','', ['id' => 'longitude']) !!}

    {!! Form::close() !!}

</div>

<script>
    navigator.geolocation.getCurrentPosition(function (position) {
        $("#latitude").val(position.coords.latitude);
        $("#longitude").val(position.coords.longitude);
        $("#nearbyForm").submit();
    });
</script>

@endsection